import apiAxios from '../api.axios';
import * as actionTypes from './action.types';
import RpiModel from '../models/rpi.model';

export const fetchRpiList = () => {
  return dispatch => {
    dispatch(fetchRpiListStart());
    apiAxios.get('rpis')
            .then( res => { dispatch(fetchRpiListSuccess(res.data)) } )
            .catch( err => { dispatch(fetchRpiListFail(err)) } )
  }
}

export function fetchRpiListStart() {
  return {
    type: actionTypes.FETCH_RPI_LIST_SUCCESS
  };
}

export const fetchRpiListSuccess = ( rpisList ) => {
  return {
      type: actionTypes.FETCH_RPI_LIST_SUCCESS,
      rpiList: rpisList
  };
};
