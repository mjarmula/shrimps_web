import apiAxios from '../api.axios';
import * as actionTypes from './action.types';

export const fetchRpiDeviceList = (rpiId) => {
  return dispatch => {
    dispatch(fetchRpiDeviceListStart());
    apiAxios.get(`rpis/${rpiId}/rpi_devices`)
            .then( res => { dispatch(fetchRpiDeviceListSuccess(res.data)) } )
            .catch( err => { dispatch(fetchRpiDeviceListFail(err)) } )
  }
}

export function fetchRpiDeviceListStart() {
  return {
    type: actionTypes.FETCH_RPI_DEVICES_LIST_SUCCESS
  };
}

export const fetchRpiDeviceListSuccess = ( rpiDevicesList ) => {
  return {
      type: actionTypes.FETCH_RPI_DEVICES_LIST_SUCCESS,
      rpiDevicesList: rpiDevicesList
  };
};

export const fetchRpiDeviceListFail = ( error ) => {
  return {
    type: actionTypes.FETCH_RPI_DEVICES_LIST_SUCCESS
  }
};
