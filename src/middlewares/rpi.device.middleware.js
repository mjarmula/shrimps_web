import { FETCH_RPI_DEVICES_LIST_SUCCESS } from '../actions/action.types';
import groupArray from 'group-array';

export const groupDevicesMiddleware = store => next => action => {
  if(action.type === FETCH_RPI_DEVICES_LIST_SUCCESS) {
    var groupped = groupArray(action.rpiDevicesList, 'config_group.name');
    action.rpiDevicesList = groupped;
  }

  next(action);
}
