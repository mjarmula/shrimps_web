import axios from 'axios';
import ApiConfig from './configs/api.config';
import { getAccessToken } from './services/auth.service';

const apiAxios = axios.create({
    baseURL: `${ApiConfig['protocol']}://${ApiConfig['hostname']}:${ApiConfig['port']}/api/${ApiConfig['version']}/`,
    headers: {  'Authorization': `Bearer ${getAccessToken()}` }
});

apiAxios.interceptors.response.use(
(response) => {
    return response;
  },
  (error) => {
    if(error.response.status == 401) {
      window.location.replace('auth');
    }

    return Promise.reject(error)
  });

export default apiAxios;
