import React, { Component } from 'react';
import App from '../containers/app.container';
import Header from '../components/header.component';
import Footer from '../components/footer.component';
import SideBar from '../components/sidebar.component';
import RpiDetails from '../containers/rpi/details.container';
import { Route, Redirect } from 'react-router-dom';
import { Row, Col } from 'react-materialize';

const MainLayout = () => (
  <div id="layout">
    <SideBar />

    <main>
      <Header />
      <Route exact path='/rpi/:id' component={ RpiDetails } />
      <div className="pusher"></div>
    </main>

    <Footer />
  </div>
)

export default MainLayout;
