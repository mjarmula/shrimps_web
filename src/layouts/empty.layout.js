import React, { Component } from 'react';
import Auth from '../containers/auth.container';

const EmptyLayout = () => (
  <div className="container">
    <Auth />
  </div>
)

export default EmptyLayout;
