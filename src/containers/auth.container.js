import React, { Component } from 'react';
import apiAxios from '../api.axios';
import { setAccessToken } from '../services/auth.service';
import { Row, Input, Button } from 'react-materialize';

class AuthComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { email: '', password: '' };
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onFormSubmit(e) {
    e.preventDefault();
    apiAxios.post('user_token', {
      auth: { email: this.state.email, password: this.state.password }
    }).then((response) => {
      setAccessToken(response.data.jwt);
      window.location.replace('/');
    }).catch((error) => {
      console.warn(error)
    })
  }

  render() {
    return(
      <Row>
        <h3>Login</h3>
        <form onSubmit={this.onFormSubmit}>
          <Input  s={6}
                  label="Email"
                  type="email"
                  onChange={(e) => { this.setState({ email: e.target.value }) }}
                  value={this.state.email} />
          <Input  s={6}
                  label="Password"
                  type="password"
                  onChange={(e) => { this.setState({ password: e.target.value }) }}
                  value={this.state.password} />
          <div>
            <Button type="submit" waves='light'>Submit</Button>
          </div>
        </form>
      </Row>
    );
  }
}

export default AuthComponent;
