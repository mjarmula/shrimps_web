import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchRpiDeviceList } from '../../actions/rpi.devices.actions';
import DeviceGroupComponent from '../../components/rpi/device.group.component'

class RpiDetailsContainer extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount () {
    var rpiId = this.props.match.params.id;
    this.props.onFetchRpiDeviceList(rpiId);
  }

  renderDeviceBox(groupedDevices, groupName) {
    return (
      <DeviceGroupComponent groupedDevices={groupedDevices} groupName={groupName} />
    );
  }

  render() {
    if(this.props.rpiDeviceList.items) {
      var devices = [];
      for(var groupName in this.props.rpiDeviceList.items) {
        var deviceObject = this.renderDeviceBox(this.props.rpiDeviceList.items[groupName], groupName);
        devices.push(deviceObject);
      }
      return devices;
    } else {
      return '';
    }
  }
}

function mapStateToProps({ rpiDeviceList }) {
  return { rpiDeviceList };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchRpiDeviceList: (rpiId) => dispatch( fetchRpiDeviceList(rpiId) )
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RpiDetailsContainer);
