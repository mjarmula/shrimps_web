import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchRpiList } from '../../actions/rpi.actions'
import { Link } from 'react-router-dom';

class RpiListContainer extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount () {
    this.props.onFetchRpiList();
  }

  renderItem(rpiItem) {
    return (
      <li key={rpiItem.id}>
        <Link to={`/rpi/${rpiItem.id}`} className="waves-effect waves-teal">
          hostname: {rpiItem.hostname}
        </Link>
      </li>
    );
  }

  render() {
    if(this.props.rpiList.items){
      return this.props.rpiList.items.map(this.renderItem.bind(this))
    } else {
      return '';
    }
  }
}

function mapStateToProps({ rpiList }) {
  return { rpiList };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchRpiList: () => dispatch( fetchRpiList() )
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RpiListContainer);
