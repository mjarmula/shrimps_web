import { combineReducers } from 'redux';
import RpiListReducer from './rpi/list.reducer';
import RpiDeviceListReducer from './rpi/device.list.reducer';

const rootReducer = combineReducers({
  rpiList: RpiListReducer,
  rpiDeviceList: RpiDeviceListReducer
});

export default rootReducer;
