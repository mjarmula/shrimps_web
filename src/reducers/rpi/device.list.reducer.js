import * as actionTypes from '../../actions/action.types';
import { updateObject } from '../utility';

const initialState = {
  items: [],
  loading: false,
};

const start = ( state, action ) => {
  return updateObject( state, { loading: true } );
};

const success = ( state, action ) => {
  return updateObject(state, {
    items: action.rpiDevicesList,
    loading: false
  });
};

export default function(state = initialState, action) {
  switch(action.type) {
    case actionTypes.FETCH_RPI_DEVICES_LIST_START: return start( state, action );
    case actionTypes.FETCH_RPI_DEVICES_LIST_SUCCESS: return success( state, action );
    default: return state;
  }

  return state;
}
