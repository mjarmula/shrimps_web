import * as actionTypes from '../../actions/action.types';
import { updateObject } from '../utility';

const initialState = {
  items: [],
  loading: false,
};

const fetchRpiListStart = ( state, action ) => {
  return updateObject( state, { loading: true } );
};

export const fetchRpiListSuccess = ( state, action ) => {
  return updateObject(state, {
    items: action.rpiList,
    loading: false
  });
};

export default function(state = initialState, action) {
  switch(action.type) {
    case actionTypes.FETCH_RPI_LIST_START: return fetchRpiListStart( state, action );
    case actionTypes.FETCH_RPI_LIST_SUCCESS: return fetchRpiListSuccess( state, action );
    default: return state;
  }

  return state;
}
