import React, { Component } from 'react';
import { Footer } from 'react-materialize';
import shrimpLogo from '../assets/images/shrimp_logo.png';
import RpiList from '../containers/rpi/list.container';

export default class SideBarComponent extends Component {
  render() {
    return(
      <div className="side-nav fixed">
        <div id="logo">
          <span className="brand-logo active">
            <img src={shrimpLogo} />
          </span>
        </div>
        <ul className="collapsible">
          <li>
            <div className="collapsible-header active">Rpi devices</div>
            <div className="collapsible-body">
              <ul>
                <RpiList />
              </ul>
            </div>
          </li>
          <li>
            <div className="collapsible-header active">Account settings</div>
            <div className="collapsible-body">
              <ul>
                <li>
                  <a className="waves-effect waves-teal">Profile</a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    );
  }
}
