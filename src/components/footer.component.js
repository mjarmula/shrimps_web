import React, { Component } from 'react';
import { Footer } from 'react-materialize';

export default class FooterComponent extends Component {
  render() {
    return(
      <Footer copyrights="Copyright © 2015 MOSURA.PL">
          <h5 className="white-text">
            <a href="http://www.mosura.pl" target="_blank">Designed by MOSURA.PL</a>
          </h5>
      </Footer>
    );
  }
}
