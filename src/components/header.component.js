import React, { Component } from 'react';
import { Navbar, NavItem, Icon } from 'react-materialize';

export default class HeaderComponent extends Component {
  render() {
    return(
      <div>
          <Navbar right>
            <NavItem href=''><Icon>language</Icon></NavItem>
            <NavItem href=''><Icon>notifications</Icon></NavItem>
            <NavItem href=''><Icon>power_settings_new</Icon></NavItem>
          </Navbar>
      </div>
    );
  }
}
