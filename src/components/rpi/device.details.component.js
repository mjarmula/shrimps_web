import React, { Component } from 'react';
import { Card } from 'react-materialize';
import DeviceDetails from './device.details.component';

export default class DeviceGroupComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <Card className='blue-grey darken-1' textClassName='white-text' title={this.props.data.name} actions={[<a href=''>Configure</a>]}>
        Last reading: <span>{this.props.data.last_reading}</span>
      </Card>
    );
  }
}
