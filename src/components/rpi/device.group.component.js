import React, { Component } from 'react';
import { Footer } from 'react-materialize';
import DeviceDetails from './device.details.component';

export default class DeviceGroupComponent extends Component {
  constructor(props) {
    super(props);
  }

  renderDeviceDetails(devieDetails) {
    return <DeviceDetails data={devieDetails} />
  }

  render() {
    return(
      <div class="device-details-group">
        <h4>{this.props.groupName}</h4>
        {this.props.groupedDevices.map(this.renderDeviceDetails)}
      </div>
    );
  }
}
