const ApiConfig = {
  hostname: 'localhost',
  port: '3000',
  protocol: 'http',
  version: 'v1'
};

export default ApiConfig;
