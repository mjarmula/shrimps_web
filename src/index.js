import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import ReduxPromise from 'redux-promise';
import thunk from 'redux-thunk';

import reducers from './reducers';

import MainLayout from './layouts/main.layout';
import EmptyLayout from './layouts/empty.layout';

import { groupDevicesMiddleware } from './middlewares/rpi.device.middleware';

const createStoreWithMiddleware = applyMiddleware(thunk, groupDevicesMiddleware)(createStore);


ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <Switch>
        <Route path='/auth' component={ EmptyLayout } />
        <Route component={ MainLayout } />
      </Switch>
    </BrowserRouter>
  </Provider>
  , document.querySelector('#container'));
